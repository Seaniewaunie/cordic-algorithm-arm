#!/bin/bash
#USAGE: ./convert.sh HALF_FILE TRIG_FILE
lookup="lookup_halves"
#Edit this line to change the name of the array
trig="lookupsin"
val1=$(cat $1 | sed -e '/^ Column/d' | grep -P ".+[^0-9]+.+" | sed 's/   /, /g' | tr '\n' 'R' | sed 's/R/,\n/g'  | cut -f2- -d ' ' | head -n -1 | sed '1d'| tr '\n' ' ' | sed 's/..$//')
val2=$(cat $2 | sed -e '/^ Column/d' | grep -P ".+[^0-9]+.+" | sed 's/   /, /g' | tr '\n' 'R' | sed 's/R/,\n/g'  | cut -f2- -d ' ' | head -n -1 | sed '1d'| tr '\n' ' ' | sed 's/..$//')

echo "float ${lookup}[] = { $val1 };" > float_halves.txt
echo "float ${trig}[] = { $val2 };" > float_trig.txt 

