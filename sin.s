	.arch armv5
	.eabi_attribute 28, 1
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 6
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.file	"sin.c"
	.section	.rodata
	.align	2
.LC0:
	.word	1119092736
	.word	1106247680
	.word	1097311715
	.word	1088801053
	.word	1080382666
	.word	1071986927
	.word	1063596306
	.word	1055207362
	.word	1046818419
	.word	1038430482
	.word	1030041337
	.word	1021652461
	.word	1013263853
	.word	1004875459
	.word	996487066
	.word	988098029
	.word	979709592
	.word	971320984
	.word	962932720
	.word	954543425
	.word	946155367
	.word	937766759
	.word	929377601
	.word	920989433
	.word	912600825
	.word	904211777
	.word	895823521
	.word	887435089
	.word	879046129
	.word	870657521
	.align	2
.LC1:
	.word	1065353216
	.word	1056964608
	.word	1048576000
	.word	1040187392
	.word	1031798784
	.word	1023410176
	.word	1015021568
	.word	1006632960
	.word	998244137
	.word	989855529
	.word	981467093
	.word	973078485
	.word	964689877
	.word	956301269
	.word	947912661
	.word	939524212
	.word	931135604
	.word	922746886
	.word	914358278
	.word	905969236
	.word	897580980
	.word	889192498
	.word	880803890
	.word	872415282
	.word	864026674
	.word	855637835
	.word	847249227
	.word	838860822
	.word	830472214
	.word	822083177
	.text
	.align	2
	.global	main
	.syntax unified
	.arm
	.fpu vfpv3-d16
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 256
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #256
	mov	r3, #10
	str	r3, [fp, #-20]
	ldr	r2, .L10
	sub	r3, fp, #140
	mov	r1, r2
	mov	r2, #120
	mov	r0, r3
	bl	memcpy
	ldr	r2, .L10+4
	sub	r3, fp, #260
	mov	r1, r2
	mov	r2, #120
	mov	r0, r3
	bl	memcpy
	mov	r3, #1065353216
	str	r3, [fp, #-16]	@ float
	ldr	r3, .L10+8
	str	r3, [fp, #-8]	@ float
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L2
.L6:
	ldr	r3, [fp, #-20]
	vmov	s15, r3	@ int
	vcvt.f32.s32	s15, s15
	vldr.32	s14, [fp, #-8]
	vcmpe.f32	s14, s15
	vmrs	APSR_nzcv, FPSCR
	blt	.L9
	ldr	r3, [fp, #-12]
	lsl	r3, r3, #2
	sub	r2, fp, #4
	add	r3, r2, r3
	sub	r3, r3, #256
	vldr.32	s15, [r3]
	vldr.32	s14, [fp, #-16]
	vsub.f32	s15, s14, s15
	vstr.32	s15, [fp, #-16]
	ldr	r3, [fp, #-12]
	lsl	r3, r3, #2
	sub	r2, fp, #4
	add	r3, r2, r3
	sub	r3, r3, #136
	vldr.32	s15, [r3]
	vldr.32	s14, [fp, #-8]
	vsub.f32	s15, s14, s15
	vstr.32	s15, [fp, #-8]
	b	.L5
.L9:
	ldr	r3, [fp, #-12]
	lsl	r3, r3, #2
	sub	r2, fp, #4
	add	r3, r2, r3
	sub	r3, r3, #256
	vldr.32	s15, [r3]
	vldr.32	s14, [fp, #-16]
	vadd.f32	s15, s14, s15
	vstr.32	s15, [fp, #-16]
	ldr	r3, [fp, #-12]
	lsl	r3, r3, #2
	sub	r2, fp, #4
	add	r3, r2, r3
	sub	r3, r3, #136
	vldr.32	s15, [r3]
	vldr.32	s14, [fp, #-8]
	vadd.f32	s15, s14, s15
	vstr.32	s15, [fp, #-8]
.L5:
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L2:
	ldr	r3, [fp, #-12]
	cmp	r3, #27
	ble	.L6
	mov	r3, #0
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, pc}
.L11:
	.align	2
.L10:
	.word	.LC0
	.word	.LC1
	.word	1119092736
	.size	main, .-main
	.ident	"GCC: (GNU) 7.0.1 20170309 (Red Hat Cross 7.0.1-0.4)"
	.section	.note.GNU-stack,"",%progbits
