main:
  stmfd sp!, {fp, lr}	@ int main()
  add fp, sp, #4
  sub sp, sp, #288
  mov r3, #50		@ Set the angle to 50
  str r3, [fp, #-28]
  sub r3, fp, #160	@Create the trig lookup table
  mov r2, #128
  mov r1, #0
  mov r0, r3
  ldr r3, .L15
  str r3, [fp, #-160] 
  ldr r3, .L15+4
  str r3, [fp, #-156] 
  ldr r3, .L15+8
  str r3, [fp, #-152] 
  ldr r3, .L15+12
  str r3, [fp, #-148] 
  ldr r3, .L15+16
  str r3, [fp, #-144] 
  ldr r3, .L15+20
  str r3, [fp, #-140] 
  ldr r3, .L15+24
  str r3, [fp, #-136] 
  ldr r3, .L15+28
  str r3, [fp, #-132] 
  ldr r3, .L15+32
  str r3, [fp, #-128] 
  ldr r3, .L15+36
  str r3, [fp, #-124] 
  ldr r3, .L15+40
  str r3, [fp, #-120] 
  ldr r3, .L15+44
  str r3, [fp, #-116] 
  ldr r3, .L15+48
  str r3, [fp, #-112] 
  ldr r3, .L15+52
  str r3, [fp, #-108] 
  ldr r3, .L15+56
  str r3, [fp, #-104] 
  ldr r3, .L15+60
  str r3, [fp, #-100] 
  ldr r3, .L15+64
  str r3, [fp, #-96] 
  ldr r3, .L15+68
  str r3, [fp, #-92] 
  ldr r3, .L15+72
  str r3, [fp, #-88] 
  ldr r3, .L15+76
  str r3, [fp, #-84] 
  ldr r3, .L15+80
  str r3, [fp, #-80] 
  ldr r3, .L15+84
  str r3, [fp, #-76] 
  ldr r3, .L15+88
  str r3, [fp, #-72] 
  ldr r3, .L15+92
  str r3, [fp, #-68] 
  ldr r3, .L15+96
  str r3, [fp, #-64] 
  ldr r3, .L15+100
  str r3, [fp, #-60] 
  ldr r3, .L15+104
  str r3, [fp, #-56] 
  ldr r3, .L15+108
  str r3, [fp, #-52] 
  ldr r3, .L15+112
  str r3, [fp, #-48] 
  ldr r3, .L15+116
  str r3, [fp, #-44] 
  sub r3, fp, #288
  mov r2, #128
  mov r1, #0
  mov r0, r3

  mov r3, #1065353216	@ Create the 2^-n lookup table
  str r3, [fp, #-288] 
  mov r3, #1056964608
  str r3, [fp, #-284] 
  mov r3, #1048576000
  str r3, [fp, #-280] 
  mov r3, #1040187392
  str r3, [fp, #-276] 
  mov r3, #1031798784
  str r3, [fp, #-272] 
  mov r3, #1023410176
  str r3, [fp, #-268] 
  mov r3, #1015021568
  str r3, [fp, #-264] 
  mov r3, #1006632960
  str r3, [fp, #-260] 
  ldr r3, .L15+120
  str r3, [fp, #-256] 
  ldr r3, .L15+124
  str r3, [fp, #-252] 
  ldr r3, .L15+128
  str r3, [fp, #-248] 
  ldr r3, .L15+132
  str r3, [fp, #-244] 
  ldr r3, .L15+136
  str r3, [fp, #-240] 
  ldr r3, .L15+140
  str r3, [fp, #-236] 
  ldr r3, .L15+144
  str r3, [fp, #-232] 
  ldr r3, .L15+148
  str r3, [fp, #-228] 
  ldr r3, .L15+152
  str r3, [fp, #-224] 
  ldr r3, .L15+156
  str r3, [fp, #-220] 
  ldr r3, .L15+160
  str r3, [fp, #-216] 
  ldr r3, .L15+164
  str r3, [fp, #-212] 
  ldr r3, .L15+168
  str r3, [fp, #-208] 
  ldr r3, .L15+172
  str r3, [fp, #-204] 
  ldr r3, .L15+176
  str r3, [fp, #-200] 
  ldr r3, .L15+180
  str r3, [fp, #-196] 
  ldr r3, .L15+184
  str r3, [fp, #-192] 
  ldr r3, .L15+188
  str r3, [fp, #-188] 
  ldr r3, .L15+192
  str r3, [fp, #-184] 
  ldr r3, .L15+196
  str r3, [fp, #-180] 
  ldr r3, .L15+200
  str r3, [fp, #-176] 
  ldr r3, .L15+204
  str r3, [fp, #-172] 
  mov r3, #1065353216	@ Create a variable to keep track of the sine of angle
  str r3, [fp, #-20] 
  mov r3, #1065353216	@ Create a variable to keep track of the cosine of angle
  str r3, [fp, #-24] 
  ldr r3, .L15			@ prev_sin = 90 (Used for algorithm)
  str r3, [fp, #-12] 
  ldr r3, .L15+208		@ prev_angle = 180 (Used for algorithm)
  str r3, [fp, #-8] 
  mov r3, #0			@ Start looping through lookup tables ( for i = 0; i < 28; i++ )
  str r3, [fp, #-16]
.L9:
  ldr r3, [fp, #-16]
  cmp r3, #27
  bgt .L2				@ Compare prev_angle to the angle we are calculating ( if(prev_angle >= angle) )
  ldr r3, [fp, #-28]
  vmov s15, r3 @ int
  vcvt.f32.s32 s14, s15
  vldr.32 s15, [fp, #-8]
  vcmpe.f32 s14, s15
  vmrs APSR_nzcv, FPSCR
  bhi .L13
  ldr r3, [fp, #-16]	@ Subtract the current value in the 2^-n table from the variable used to store the cosine of the angle
  mov r3, r3, asl #2
  sub r2, fp, #4
  add r3, r2, r3
  sub r3, r3, #284
  vldr.32 s15, [r3]
  vldr.32 s14, [fp, #-20]
  vsub.f32 s15, s14, s15
  vstr.32 s15, [fp, #-20]
  ldr r3, [fp, #-16]	@ Subtract the current value in the trig lookup table from the variable used to store the cosine of the angle
  mov r3, r3, asl #2
  sub r2, fp, #4
  add r3, r2, r3
  sub r3, r3, #156
  vldr.32 s15, [r3]
  vldr.32 s14, [fp, #-8]
  vsub.f32 s15, s14, s15
  vstr.32 s15, [fp, #-8]
  b .L5
.L13:					@ If conditional on line 149 fails
  ldr r3, [fp, #-16]	@ Add the current value in the 2^-n table to the variable used to store the cosine of the angle
  mov r3, r3, asl #2
  sub r2, fp, #4
  add r3, r2, r3
  sub r3, r3, #284
  vldr.32 s15, [r3]
  vldr.32 s14, [fp, #-20]
  vadd.f32 s15, s14, s15
  vstr.32 s15, [fp, #-20]
  ldr r3, [fp, #-16]	@ Add the current value in the trig lookup table to the variable used to store the cosine of the angle
  mov r3, r3, asl #2
  sub r2, fp, #4
  add r3, r2, r3
  sub r3, r3, #156
  vldr.32 s15, [r3]
  vldr.32 s14, [fp, #-8]
  vadd.f32 s15, s14, s15
  vstr.32 s15, [fp, #-8]
.L5:
  ldr r3, [fp, #-28]	@ Here we do the same thing that we just did but for sine instead of cosine
  vmov s15, r3 @ int
  vcvt.f32.s32 s14, s15
  vldr.32 s15, [fp, #-12]
  vcmpe.f32 s14, s15
  vmrs APSR_nzcv, FPSCR
  bhi .L14
  ldr r3, [fp, #-16]
  mov r3, r3, asl #2
  sub r2, fp, #4
  add r3, r2, r3
  sub r3, r3, #284
  vldr.32 s15, [r3]
  vldr.32 s14, [fp, #-24]
  vsub.f32 s15, s14, s15
  vstr.32 s15, [fp, #-24]
  ldr r3, [fp, #-16]
  mov r3, r3, asl #2
  sub r2, fp, #4
  add r3, r2, r3
  sub r3, r3, #156
  vldr.32 s15, [r3]
  vldr.32 s14, [fp, #-12]
  vsub.f32 s15, s14, s15
  vstr.32 s15, [fp, #-12]
  b .L8
.L14:
  ldr r3, [fp, #-16]
  mov r3, r3, asl #2
  sub r2, fp, #4
  add r3, r2, r3
  sub r3, r3, #284
  vldr.32 s15, [r3]
  vldr.32 s14, [fp, #-24]
  vadd.f32 s15, s14, s15
  vstr.32 s15, [fp, #-24]
  ldr r3, [fp, #-16]
  mov r3, r3, asl #2
  sub r2, fp, #4
  add r3, r2, r3
  sub r3, r3, #156
  vldr.32 s15, [r3]
  vldr.32 s14, [fp, #-12]
  vadd.f32 s15, s14, s15
  vstr.32 s15, [fp, #-12]
.L8:
  ldr r3, [fp, #-16]
  add r3, r3, #1
  str r3, [fp, #-16]
  b .L9
.L2:
  vldr.32 s15, [fp, #-20]	@ Our algorithm returns the cosine as a negative value, so now we negate it to make it positive again
  vneg.f32 s15, s15
  vstr.32 s15, [fp, #-20]
  vldr.32 s13, [fp, #-24]	@ Calculate the tangent of the angle dy dividing sin and cosine
  vldr.32 s14, [fp, #-20]
  vdiv.f32 s15, s13, s14
  vstr.32 s15, [fp, #-32]
  mov r3, #0				@ Terminate the program gracefully
  mov r0, r3
  sub sp, fp, #4
  ldmfd sp!, {fp, lr}
  bx lr
.L15:
  .word 1119092736
  .word 1106247680
  .word 1097311715
  .word 1088801053
  .word 1080382666
  .word 1071986927
  .word 1063596306
  .word 1055207362
  .word 1046818419
  .word 1038430482
  .word 1030041337
  .word 1021652461
  .word 1013263853
  .word 1004875459
  .word 996487066
  .word 988098029
  .word 979709592
  .word 971320984
  .word 962932720
  .word 954543425
  .word 946155367
  .word 937766759
  .word 929377601
  .word 920989433
  .word 912600825
  .word 904211777
  .word 895823521
  .word 887435089
  .word 879046129
  .word 870657521
  .word 998244137
  .word 989855529
  .word 981467093
  .word 973078485
  .word 964689877
  .word 956301269
  .word 947912661
  .word 939524212
  .word 931135604
  .word 922746886
  .word 914358278
  .word 905969236
  .word 897580980
  .word 889192498
  .word 880803890
  .word 872415282
  .word 864026674
  .word 855637835
  .word 847249227
  .word 838860822
  .word 830472214
  .word 822083177
  .word 1127481344