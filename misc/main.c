//#include <stdlib.h>
#include <stdio.h>
//#include <string.h>

#include "cordic_maybe.c"

int main(int argc, char *argv[]) {
    
    float sin_result;
    float cos_result;
    float tan_result;
    //float exp_result;

    //int theta = atoi(argv[1]);

    float theta = 45;

//    printf("theta is %f\n", theta);
    
    cordic_sincos(theta, 16, &sin_result, &cos_result, &tan_result);

    printf("sine: %f\ncos: %f\ntan: %f\n", sin_result, cos_result, tan_result);

//    printf("eulers: %f\n", exp_result);

    return 0;
}
