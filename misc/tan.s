	.arch armv5
	.eabi_attribute 28, 1
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 6
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.file	"tan.c"
	.section	.rodata
	.align	2
.LC0:
	.word	1110704128
	.word	1104465101
	.word	1096810496
	.word	1088631603
	.word	1080452710
	.word	1072064102
	.word	1063675494
	.word	1053609165
	.word	1045220557
	.word	1036831949
	.align	2
.LC1:
	.word	1056964608
	.word	1048576000
	.word	1040187392
	.word	1031798784
	.word	1023410176
	.word	1015022105
	.word	1006632960
	.word	998244352
	.word	989855744
	.text
	.align	2
	.global	main
	.syntax unified
	.arm
	.fpu vfpv3-d16
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #88
	mov	r3, #45
	str	r3, [fp, #-16]
	ldr	r3, .L10
	sub	ip, fp, #56
	mov	lr, r3
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldm	lr, {r0, r1}
	stm	ip, {r0, r1}
	ldr	r3, .L10+4
	sub	ip, fp, #92
	mov	lr, r3
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr]
	str	r3, [ip]
	mov	r3, #1056964608
	str	r3, [fp, #-12]	@ float
	mov	r3, #1
	str	r3, [fp, #-8]
	b	.L2
.L6:
	ldr	r3, [fp, #-16]
	vmov	s15, r3	@ int
	vcvt.f32.s32	s14, s15
	ldr	r3, [fp, #-8]
	lsl	r3, r3, #2
	sub	r2, fp, #4
	add	r3, r2, r3
	sub	r3, r3, #52
	vldr.32	s15, [r3]
	vcmpe.f32	s14, s15
	vmrs	APSR_nzcv, FPSCR
	bpl	.L9
	ldr	r3, [fp, #-8]
	lsl	r3, r3, #2
	sub	r2, fp, #4
	add	r3, r2, r3
	sub	r3, r3, #88
	vldr.32	s15, [r3]
	vldr.32	s14, [fp, #-12]
	vsub.f32	s15, s14, s15
	vstr.32	s15, [fp, #-12]
	b	.L5
.L9:
	ldr	r3, [fp, #-8]
	lsl	r3, r3, #2
	sub	r2, fp, #4
	add	r3, r2, r3
	sub	r3, r3, #88
	vldr.32	s15, [r3]
	vldr.32	s14, [fp, #-12]
	vadd.f32	s15, s14, s15
	vstr.32	s15, [fp, #-12]
.L5:
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L2:
	ldr	r3, [fp, #-8]
	cmp	r3, #9
	ble	.L6
	mov	r3, #0
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, pc}
.L11:
	.align	2
.L10:
	.word	.LC0
	.word	.LC1
	.size	main, .-main
	.ident	"GCC: (GNU) 7.0.1 20170309 (Red Hat Cross 7.0.1-0.4)"
	.section	.note.GNU-stack,"",%progbits
