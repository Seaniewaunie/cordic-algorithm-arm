//this is the attempt to see how a tan() lookup table would work
#include <stdio.h>
int main(){

  int angle = 20;

  float lookuptan[] = {45,26.565,14.036,7.125,3.576,1.79,.895,.448,.2,.1};
  float lookup_halves[] = {1,.5,.25,.125,.0625,.03125,.015626,.0078125,.00390625,.001953125};
  int i;
  float sum1 = angle;
  float sum2 = 0;
  
  for(i=0;i<9;i++){
    if( sum1 >= 0){
		sum1 -= lookuptan[i];
		sum2 -= lookup_halves[i];
		printf("sub \n");
    }else{
		sum1 += lookuptan[i+1];
		sum2 += lookup_halves[i+1];
		printf("add \n");
    }
  }
  printf("Tan of angle is: %f \n", sum2);
  printf("Angle is: %f \n", sum1);
  
}
