halfs = @(x) 0.9 .^ x;

x = [0:80]

%plot(x, halfs(x), 'Linewidth', 2)


diary('HALFS')
diary on
halfs(x)
diary off

diary('INV_SIN')
diary on
abs(asind(halfs(x)))
diary off

%plot(x, a);


diary('INV_COS')
diary on
abs(acosd(halfs(x)))
diary off

diary('INV_TAN')
diary on
abs(atand(halfs(x)))
diary off
