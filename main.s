	.arch armv5
	.eabi_attribute 28, 1
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 6
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.file	"main.c"
	.global	atantable
	.data
	.align	2
	.type	atantable, %object
	.size	atantable, 64
atantable:
	.word	16384
	.word	9672
	.word	5110
	.word	2594
	.word	1302
	.word	651
	.word	325
	.word	162
	.word	81
	.word	41
	.word	20
	.word	10
	.word	5
	.word	3
	.word	2
	.word	1
	.text
	.align	2
	.global	cordic_sincos
	.syntax unified
	.arm
	.fpu vfpv3-d16
	.type	cordic_sincos, %function
cordic_sincos:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r4, fp}
	add	fp, sp, #4
	sub	sp, sp, #56
	str	r0, [fp, #-48]
	str	r2, [fp, #-56]
	str	r3, [fp, #-60]
	mov	r3, r1
	strb	r3, [fp, #-49]
	ldr	r3, .L24
	str	r3, [fp, #-32]
	ldrb	r3, [fp, #-49]	@ zero_extendqisi2
	cmp	r3, #16
	movcc	r3, r3
	movcs	r3, #16
	strb	r3, [fp, #-49]
	b	.L2
.L3:
	ldr	r3, [fp, #-48]
	add	r3, r3, #180
	str	r3, [fp, #-48]
.L2:
	ldr	r3, [fp, #-48]
	cmn	r3, #180
	blt	.L3
	b	.L4
.L5:
	ldr	r3, [fp, #-48]
	sub	r3, r3, #180
	str	r3, [fp, #-48]
.L4:
	ldr	r3, [fp, #-48]
	cmp	r3, #180
	bgt	.L5
	ldr	r3, [fp, #-48]
	cmn	r3, #90
	bge	.L6
	ldr	r3, [fp, #-48]
	add	r3, r3, #180
	str	r3, [fp, #-48]
	mvn	r3, #0
	str	r3, [fp, #-24]
	b	.L7
.L6:
	ldr	r3, [fp, #-48]
	cmp	r3, #90
	ble	.L8
	ldr	r3, [fp, #-48]
	sub	r3, r3, #180
	str	r3, [fp, #-48]
	mvn	r3, #0
	str	r3, [fp, #-24]
	b	.L7
.L8:
	mov	r3, #1
	str	r3, [fp, #-24]
.L7:
	ldr	r3, [fp, #-48]
	cmn	r3, #45
	bge	.L9
	ldr	r3, [fp, #-48]
	add	r3, r3, #90
	str	r3, [fp, #-48]
	mvn	r3, #0
	str	r3, [fp, #-28]
	b	.L10
.L9:
	ldr	r3, [fp, #-48]
	cmp	r3, #45
	ble	.L11
	ldr	r3, [fp, #-48]
	sub	r3, r3, #90
	str	r3, [fp, #-48]
	mov	r3, #1
	str	r3, [fp, #-28]
	b	.L10
.L11:
	mov	r3, #0
	str	r3, [fp, #-28]
.L10:
	ldr	r3, [fp, #-48]
	cmp	r3, #0
	bge	.L12
	ldr	r3, [fp, #-48]
	rsb	r3, r3, #0
	str	r3, [fp, #-48]
	ldr	r3, [fp, #-48]
	lsl	r2, r3, #10
	ldr	r1, .L24+4
	umull	r3, r4, r2, r1
	sub	r2, r2, r4
	lsr	r2, r2, #1
	add	r3, r2, r4
	lsr	r3, r3, #5
	str	r3, [fp, #-48]
	ldr	r3, [fp, #-48]
	lsl	r3, r3, #4
	str	r3, [fp, #-48]
	ldr	r3, [fp, #-48]
	rsb	r3, r3, #0
	str	r3, [fp, #-48]
	b	.L13
.L12:
	ldr	r3, [fp, #-48]
	lsl	r2, r3, #10
	ldr	r1, .L24+4
	umull	r3, r4, r2, r1
	sub	r2, r2, r4
	lsr	r2, r2, #1
	add	r3, r2, r4
	lsr	r3, r3, #5
	str	r3, [fp, #-48]
	ldr	r3, [fp, #-48]
	lsl	r3, r3, #4
	str	r3, [fp, #-48]
.L13:
	ldr	r3, .L24+8
	str	r3, [fp, #-12]
	mov	r3, #0
	str	r3, [fp, #-16]
	mov	r3, #0
	str	r3, [fp, #-8]
	mov	r3, #1
	str	r3, [fp, #-36]
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L14
.L19:
	ldr	r2, [fp, #-48]
	ldr	r3, [fp, #-8]
	sub	r3, r2, r3
	cmp	r3, #0
	ble	.L15
	mov	r3, #1
	b	.L16
.L15:
	mvn	r3, #0
.L16:
	str	r3, [fp, #-36]
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	bge	.L17
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	asr	r3, r2, r3
	ldr	r2, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-40]
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-20]
	asr	r3, r2, r3
	ldr	r2, [fp, #-16]
	sub	r3, r2, r3
	str	r3, [fp, #-16]
	ldr	r3, [fp, #-40]
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-32]
	add	r2, r3, #4
	str	r2, [fp, #-32]
	ldr	r3, [r3]
	ldr	r2, [fp, #-8]
	sub	r3, r2, r3
	str	r3, [fp, #-8]
	b	.L18
.L17:
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	asr	r3, r2, r3
	ldr	r2, [fp, #-12]
	sub	r3, r2, r3
	str	r3, [fp, #-40]
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-20]
	asr	r3, r2, r3
	ldr	r2, [fp, #-16]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	ldr	r3, [fp, #-40]
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-32]
	add	r2, r3, #4
	str	r2, [fp, #-32]
	ldr	r3, [r3]
	ldr	r2, [fp, #-8]
	add	r3, r2, r3
	str	r3, [fp, #-8]
.L18:
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L14:
	ldrb	r3, [fp, #-49]	@ zero_extendqisi2
	ldr	r2, [fp, #-20]
	cmp	r2, r3
	blt	.L19
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bge	.L20
	ldr	r3, [fp, #-12]
	rsb	r3, r3, #0
	str	r3, [fp, #-12]
.L20:
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	ble	.L21
	ldr	r3, [fp, #-56]
	ldr	r2, [fp, #-12]
	str	r2, [r3]
	ldr	r3, [fp, #-16]
	rsb	r2, r3, #0
	ldr	r3, [fp, #-60]
	str	r2, [r3]
	b	.L22
.L21:
	ldr	r3, [fp, #-28]
	cmp	r3, #0
	bge	.L23
	ldr	r3, [fp, #-12]
	rsb	r2, r3, #0
	ldr	r3, [fp, #-56]
	str	r2, [r3]
	ldr	r3, [fp, #-60]
	ldr	r2, [fp, #-16]
	str	r2, [r3]
	b	.L22
.L23:
	ldr	r3, [fp, #-56]
	ldr	r2, [fp, #-16]
	str	r2, [r3]
	ldr	r3, [fp, #-60]
	ldr	r2, [fp, #-12]
	str	r2, [r3]
.L22:
	ldr	r3, [fp, #-56]
	ldr	r3, [r3]
	ldr	r2, [fp, #-24]
	mul	r1, r3, r2
	ldr	r3, [fp, #-56]
	str	r1, [r3]
	ldr	r3, [fp, #-60]
	ldr	r3, [r3]
	ldr	r2, [fp, #-24]
	mul	r1, r3, r2
	ldr	r3, [fp, #-60]
	str	r1, [r3]
	nop
	sub	sp, fp, #4
	@ sp needed
	pop	{r4, fp}
	bx	lr
.L25:
	.align	2
.L24:
	.word	atantable
	.word	1813430637
	.word	19898
	.size	cordic_sincos, .-cordic_sincos
	.align	2
	.global	main
	.syntax unified
	.arm
	.fpu vfpv3-d16
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #24
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	mov	r3, #45
	str	r3, [fp, #-8]
	sub	r3, fp, #16
	sub	r2, fp, #12
	mov	r1, #16
	ldr	r0, [fp, #-8]
	bl	cordic_sincos
	mov	r3, #0
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, pc}
	.size	main, .-main
	.ident	"GCC: (GNU) 7.0.1 20170309 (Red Hat Cross 7.0.1-0.4)"
	.section	.note.GNU-stack,"",%progbits
