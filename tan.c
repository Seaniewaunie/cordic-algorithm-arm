#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]){

  int angle = atoi(argv[1]);
  float prev_angle;

float lookupcos[] = { 9.0000e+01, 3.0000e+01, 1.4478e+01, 7.1808e+00, 3.5833e+00, 1.7908e+00, 8.9528e-01, 4.4763e-01, 2.2381e-01, 1.1191e-01, 5.5953e-02, 2.7976e-02, 1.3988e-02, 6.9941e-03, 3.4971e-03, 1.7485e-03, 8.7426e-04, 4.3713e-04, 2.1857e-04, 1.0928e-04, 5.4642e-05, 2.7321e-05, 1.3660e-05, 6.8302e-06, 3.4151e-06, 1.7075e-06, 8.5377e-07, 4.2689e-07, 2.1344e-07, 1.0672e-07 };

float lookup_halves[] = { 1.0000e+00, 5.0000e-01, 2.5000e-01, 1.2500e-01, 6.2500e-02, 3.1250e-02, 1.5625e-02, 7.8125e-03, 3.9062e-03, 1.9531e-03, 9.7656e-04, 4.8828e-04, 2.4414e-04, 1.2207e-04, 6.1035e-05, 3.0518e-05, 1.5259e-05, 7.6294e-06, 3.8147e-06, 1.9073e-06, 9.5367e-07, 4.7684e-07, 2.3842e-07, 1.1921e-07, 5.9605e-08, 2.9802e-08, 1.4901e-08, 7.4506e-09, 3.7253e-09, 1.8626e-09 };
  int i;
  float cos_of_angle = 1;//lookup_halves[0]; // corresponding to lookup halves
  
  prev_angle = 180;//lookupcos[0];
  
  //printf("prev angle: %f\n", prev_angle);

  for(i=0;i<28;i++){
    if(prev_angle >= angle){
      //printf("cos_of_angle is %f\n", cos_of_angle);
      cos_of_angle -= lookup_halves[i];
      
      //printf("prev angle: %f\n", prev_angle);
      prev_angle -= lookupcos[i];
      
    }
    else{
      //printf("cos_of_angle is %f\n", cos_of_angle);
      cos_of_angle += lookup_halves[i];
      
      //printf("prev angle: %f\n", prev_angle);
      prev_angle += lookupcos[i];
      
    }
  }
  cos_of_angle = -cos_of_angle;
  printf("%f, ", cos_of_angle);
  //printf("final angle: %f\n", prev_angle);
}
